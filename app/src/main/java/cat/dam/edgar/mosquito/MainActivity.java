package cat.dam.edgar.mosquito;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import java.util.Random;

public class MainActivity extends AppCompatActivity
{
    private ImageButton ib_mosquito;
    private AnimationDrawable animation;
    private Animation fly;
    private Random r;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
        startGame();
    }

    private void setVariables()
    {
        ib_mosquito = (ImageButton) findViewById(R.id.ib_mosquito);
        fly = AnimationUtils.loadAnimation(this, R.anim.mosquito_fly);
        animation = new AnimationDrawable();
        r = new Random();
    }

    private void setListeners()
    {
        ib_mosquito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                killMosquito();
            }
        });
    }

    private void startGame()
    {
        ib_mosquito.setBackgroundResource(R.drawable.mosquito);
        ib_mosquito.setX(r.nextInt(700));
        ib_mosquito.setY(r.nextInt(700));
        ib_mosquito.startAnimation(fly);
        startAnimation();
    }

    private void startAnimation()
    {
        animation = (AnimationDrawable) ib_mosquito.getBackground();
        animation.start();
    }

    private void killMosquito()
    {
        ib_mosquito.setBackgroundResource(R.drawable.blood);
        ib_mosquito.clearAnimation();
        startAnimation();

        // Wait 1'5sec until start another game to see how mosquito die
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() { startGame(); }
        }, 1500);
    }
}